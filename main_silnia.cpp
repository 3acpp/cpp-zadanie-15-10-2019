/* ZADANIE DO WYKONANIA:
 * - dodać potęgowanie (o dowolny stopień)
 * - dodać pierwiastkowanie (o dowolny stopień)
 * - dodać silnię
 * - dodać modulo
 *
 * Spróbować znaleźć i zaimplemnotwać pętlę, dzięki
 * której program będzie działał w nieskończoność
 * (wyłączenie wraz z oknem aplikacji)
 * */
#include <iostream>
#include <string>
//<cmath>

int main()
{
    int a=0,b=0;
    char operacja='+';
    //int liczba1,liczba2;
    for(;;)
    {

        std::cout << "Program do obliczen arytmetycznych\n" <<
                     "Podaj liczbe a: ";
        std::cin >> a;
        std::cout << "Podaj liczbe b: ";
        std::cin >> b;
        std::cout << "Podaj operacje do wykonania: ";
        std::cin >> operacja;

        /*
         * Warunki zawsze muszą kończyć się prawdą
         * Warunki mogą być ze sobą łączone
         * >= - większe lub równe, np. 4>=4 to prawda, ale 4>=5 już nie
         * <= - mniejsze lub równe, np. 4<=5 to prawda, ale 4<=2 już nie
         * < - mniejsze, np. 4<5 prawda, ale 4<3 już nie
         * > większe, np. 5>4 prawda, ale 4>5 już nie
         * == - równe, np. 5==5 prawda, ale 4 == 5 już nie
         * != - różne/nie równe, np. 4 != 5 będzie prawdą, ale 5!=5 już nie
         * */
        /* Poniższy warunek wykona się o ile zostanie spełnione
         * wyrażenie (da efekt prawdy); jeżeli nie - kod w
         * nawiasach klamrowych zostanie pominięty
         * */
        if(operacja=='+')
        {
            std::cout << "Sumowanie: a + b = " << a+b << std::endl;
        }
        /*poniżej przykład operacji if...else if czyli kaskadowe
         * wykonywanie warunków. Jeżeli warunek pierwszy nie
         * zostanie spełniony to wykona sie kolejny, natomiast
         * spełnienie warunku numer 1 powoduje pominięcie
         * kolejnych warunków else if (nawet gdyby teżpasowały)
         * */
        if(operacja=='-')
        {
            std::cout << "Odejmowanie: a - b = " << (a-b) << std::endl;
        }
        /*
         * Tutaj przykład pokazujący nie wykonania się kodu
         * gdy użytkownik wpisze znak -. Warunek zostanie pominięty
         * ponieważ poprzedni warunek (wyżej) wykonywał się
         * tylko wtedy gdy użytkownik podał działanie odejmowania.
         * W poniższym warunku symbol || reprezentuje logiczną
         * sumę (OR) co znaczy, że warunek się wykona gdy
         * dowolne działanie da efekt prawdy (brmka OR)
         * W zwiażku z tym warunek wykona się gdy użytkownik
         * będzie chciał dokonać mnożenia
         * */
        else if (operacja=='*' || operacja=='-')
        {
            std::cout << "Mnozenie: a * b = " << (a*b) << std::endl;
        }
        /*
         * Przykład łączenia warunków poprzez symbol && (AND).
         * Poniższy kod wykona się się jedynie w przypadku
         * gdy WSZYSTKIE warunki zakończą się sukcesem
         * (działanie bramki AND). Jeżeli if się nie wykona
         * to wykona się kod dostępny w klauzuli else
         * (np. jako obsługa błędu).
         * */
        if (operacja=='/' && b!=0)
        {
            std::cout << "Dzielenie: a / b = " << a/b << std::endl;
        }
        if (operacja=='!')
        {
            unsigned long long wynik=1;
            //5! = 1*2*3*4*5
            //ogólna zasada dzialania pętli for
            //int i = 0;
            //czy i<a? jeżeli tak - kontynuuj działanie pętli
            //przy każdym wykonaniu pętli zwiększ i o 1
            for(int i=1;i<=a;i=i+1)
            {
                wynik=wynik*i;
            }

            std::cout << "Silnia a: " << wynik << std::endl;
        }
        /*pierwiastki, potęgi*/
        /* pow(double,double) */
        /*modulo*/
        /* wynik = a % b <- reszta z dzielenia!
         * */


    }

    return 0;
}
